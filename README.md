

# 1. Customer Management Service:
Wallet will be created on Fireblocks. Softledger will keep the relationship of customer to wallet. One Customer can have many wallets. 

   ## 1.1 Getting Wallet For Customer.
The following request will query softledger to pull up wallets information. If no wallet is found it will return **402 Not found**

### Request
```http
GET /cms/v1/wallet/:cifid/:symbol HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json
```


### Response
**Status Code:** 200 Success:
**Response Body:**
```json
[
	{
	  "cifid": "same-as-specified-in-request",
	  "symbol": "same-as-specified-in-request",
	  "created_at": 1631707952002,
	  "address": "rs9UtmvcwLp3mm2cQTTSDXRsAUaf9iRYER",
	  "tag": "3848128875",
	  "type": "Deposit",
	  "legacyAddress": "",
	  "enterpriseAddress": ""
	}
]
```

```mermaid
sequenceDiagram
CoreService ->> CMS: GET /ums/v1/wallet/:cifid/:symbol
CMS-->> Softledger: Get All wallets associated with :cifid and :symbol.
CMS->> CoreService: Response
```

## 1.2 Creating Wallet for Customer:

To create wallet for customer following API should will generate it. It will create wallet and Fireblock and define associate this wallet address with cifid in softledger.

### Request
```http
POST /cms/v1/wallet/:cifid HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json

{
	"cifid": "<cifid>",
	"symbol": "BTC"
}
```

### Response
**Status Code:** 200 Success:
**Response Body:**
```json
{
  "cifid": "same-as-specified-in-request",
  "symbol": "same-as-specified-in-request",
  "created_at": 1631707952002,
  "address": "rs9UtmvcwLp3mm2cQTTSDXRsAUaf9iRYER",
  "tag": "3848128875",
  "type": "Deposit",
  "legacyAddress": "",
  "enterpriseAddress": ""
}
```

```mermaid
sequenceDiagram
CoreService ->> CMS: POST /ums/v1/wallet/:cifid
CMS-->>Fireblock: Upsert Asset in Vaults.
CMS-->>Fireblock: Generate Address under the Asset.
CMS-->> Softledger: Associate Address with Cifid.
Note left of Softledger: This will create following entries<br/>1-Location<br/>, 2-Customer<br/> 3-Wallet<br/> 4-Symbol<br/>
CMS->> CoreService: Response 
```


# 2. Financial Management Services:
## 2.1 Checking Balances for Customer:
Following will calculate available balances for customer base on specified symbol.

### Request

```http
GET /fms/v1/balance/:cifid/:symbol HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json
```

### Response
**Status Code:** 200 Success:
**Response Body:**
```json
[
	{
	  "cifid": "same-as-specified-in-request",
	  "symbol": "same-as-specified-in-request",
	  "created_at": 1631707952002,
	  "total": 5
	}
]
```

If we want to know all the crypto balances for a customer, request will be:

### Request
```http
GET /fms/v1/balance/:cifid HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json
```
### Response
**Status Code:** 200 Success:
**Response Body:**
```json
[
	{
	  "cifid": "same-as-specified-in-request",
	  "symbol": "BTC",
	  "created_at": 1631707952002,
	  "total": 5
	},
	{
	  "cifid": "same-as-specified-in-request",
	  "symbol": "ETH",
	  "created_at": 1631707952002,
	  "total": -10
	}
]
```

```mermaid
sequenceDiagram
CoreService ->> FMS: GET /fms/v1/balance/:cifid
FMS-->> Softledger: Get All crypto transactions by :cifid.
loop 
	FMS->>FMS: Accomulate All Transaction 
end
FMS->> CoreService: Response
```
NOTE: Should consider the option to cache snapshot of balances @ X time, and mark net balance.


## 2.2 Recording Transaction For Customer:
### 2.2.1 Transfer In / Deposit:
Step 2.2.4 Syncing Up Ledgers should record deposits.

### 2.2.2 Transfer Out / Withdrawal:

Following API Call will register transfer out/withdrawal request. The Withdraw will be done on Pool Account in Fireblock and it's ledger entry will be made in Softledger that will fall under the customer wallet transaction (Which Wallet is the question here?). Once a request is registered We can track the status of transaction by calling the status endpoint:

#### Request
```http
POST /fms/v1/withdraw HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json

{
	"cifid": "<cifid>",
	"symbol": "BTC",
	"destination": {
		"type": "ONE_TIME_ADDRESS",
		"one_time_address": {
			"address": "37NFX8KWAQbaodUG6pE1hNUH1dXgkpzbyZ"
		}
	},
	"amount": "0.9",
	"note": "Created by API SDK",
	"ifsg_fee": "0.1"
}
```

#### Response
```json
{
	"id": "ae0a299a-b1a1-410f-87db-d3bad3f77421",
	"status": "SUBMITTED"
}
```

```mermaid
sequenceDiagram
CoreService ->> FMS: POST /fms/v1/withdraw
FMS-->> Softledger: Add tx record to freeze balance.
FMS-->> Fireblock: Register Withdrawal Request.
FMS-->> CoreService: Return ID of the Request with initial Status SUBMITTED. 
loop Check Status Update Of TX
	CoreService->>FMS: GET /fms/v1/status/:txId
	Note right of CoreService: Once Status is<br/> 1- COMPLETED<br/>, 2-CANCELLED<br/> 3-REJECTED<br/> 4-BLOCKED<br/> 5- FAILED
	FMS--> CoreService: Stop loop
end
```

##### All Status And their Description:
| Status | description |
|:---|:-----|
| SUBMITTED | The transaction was submitted to the Fireblocks system and is being processed |
| QUEUED | Transaction is queued. Pending for another transaction to be processed |
| PENDING_AUTHORIZATION | The transaction is pending authorization by other users (as defined in the policy) |
| PENDING_SIGNATURE | The transaction is pending the initiator to sign the transaction |
| BROADCASTING | The transaction is pending broadcast to the blockchain network |
| PENDING_3RD_PARTY_MANUAL_APPROVAL | The transaction is pending manual approval as required by the 3rd party, usually an email approval |
| PENDING_3RD_PARTY | The transaction is pending approval by the 3rd party service (e.g exchange) |
| CONFIRMING | Pending confirmation on the blockchain |
| PARTIALLY_COMPLETED | (Only for Aggregated transactions) One or more of of the transaction records have completed successfully |
| PENDING_AML_SCREENING | In case the AML screening feature is enabled, transaction is pending AML screening result |
| COMPLETED | Successfully completed |
| CANCELLED | The transaction was cancelled or rejected by the user on the Fireblocks platform or by the 3rd party service from which the funds are withdrawn |
| REJECTED | The transaction was rejected by the Fireblocks system or by the 3rd party service |
| BLOCKED | The transaction was blocked due to a policy rule |
| FAILED | The transaction has failed |


### 2.2.3 Status Of Transaction:
Following request is to provide the detail status of transaction

#### Request:
```http
GET /fms/v1/status/:txId HTTP/1.1
Authorization: jwt.token.verify
Host: geniomeqa.ifsgdev.io
Content-Type: application/json
```
#### Response:
```json
{
  "id": ":txId",
  "createdAt": 1632139496837,
  "lastUpdated": 1632139498230,
  "assetId": "BTC_TEST",
  "source": {
    "id": "0",
    "type": "VAULT_ACCOUNT",
    "name": "Default",
    "subType": ""
  },
  "destination": {
    "type": "ONE_TIME_ADDRESS",
    "name": "N/A",
    "subType": ""
  },
  "amount": 0.00001,
  "fee": -1,
  "networkFee": -1,
  "netAmount": -1,
  "sourceAddress": "",
  "destinationAddress": "37NFX8KWAQbaodUG6pE1hNUH1dXgkpzbyZ",
  "destinationAddressDescription": "",
  "destinationTag": "",
  "status": "BLOCKED",
  "txHash": "",
  "subStatus": "BLOCKED_BY_POLICY",
  "signedBy": [],
  "createdBy": "8e633763-3dae-5b8c-9337-8e0e06e5c30e",
  "rejectedBy": "",
  "amountUSD": 0.44,
  "addressType": "",
  "note": "Created by API SDK",
  "exchangeTxId": "",
  "requestedAmount": 0.00001,
  "feeCurrency": "BTC_TEST",
  "operation": "TRANSFER",
  "amountInfo": {
    "amount": "0.00001",
    "requestedAmount": "0.00001",
    "amountUSD": "0.44"
  },
  "feeInfo": {},
  "destinations": [],
  "blockInfo": {},
  "signedMessages": []
}
```

```mermaid
sequenceDiagram
CoreService ->> FMS: GET /fms/v1/status/:txId
FMS-->> Fireblock: Get Tx Status.
FMS->> CoreService: Response
```

### 2.2.4 Syncing Up Ledgers.
Webhook (RealTime)
```mermaid
sequenceDiagram
Fireblock ->> FMS: POST /fms/v1/webhook
loop Update Ledger
	FMS-->>Softledger: If it's deposit add tx update.
	FMS-->>Softledger: If it's withdrawal unfreeze balance if needed.
end
FMS->> CoreService: callback
```

Polling Tx (Schedule)
```mermaid
sequenceDiagram
FMS-->> Softledger: Grab last recorded transaction time.
FMS->> Fireblock: GET /v1/transactions
FMS-->> Softledger: Consume transactions.
```
NOTE: Discuss where should we store the pointer for last pooled batch.


# 3. Trading Service:
## 3.1 Trade Life Cycle
```mermaid
sequenceDiagram
CoreService->>CryptoApp: GET /v1/symbols
CryptoApp-->> OSF: Query for supported symbols
CryptoApp->>CoreService: Response

CoreService-->OSF: "Step2: Get Rate"

CoreService->>CryptoApp: POST /v1/prices
Note right of CoreService: specify request UUID
CryptoApp-->> OSF: Query to BUY 100 BTC/USD

OSF-->> CryptoApp: Query For BUY 100 BTC/USD
CryptoApp ->> CoreService: Response

CoreService-->OSF: "Step3: Confirm Execution"

CoreService->>CryptoApp: POST /v1/orders
Note right of CoreService: same request UUID is specified<br/> to confirm execution
OSF-->> CryptoApp: Confirmed BUY 100 BTC/USD
CryptoApp ->> CoreService: Response
```

## 3.2 Settlement (TODO)
This is settlement between IFSG pool account to OSF. THE API specs can be found in [https://osf.stoplight.io/docs/swagger-ifsg-import](https://osf.stoplight.io/docs/swagger-ifsg-import/YXBpOjE1MTE2NjQ0-geniome-api)

REF: https://osf.stoplight.io/docs/swagger-ifsg-import/YXBpOjE1MTE2NjQ0-geniome-api
